# Challenge Go with Docker

## Run application
- Open terminal and run command: `docker run pedrosouza44/codeeducation`.

## Run application locally
- Open terminal in project dist and run command: `go run main.go`.

### Requirements
- Docker and docker-compose instaled