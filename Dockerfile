FROM golang:alpine AS builder

WORKDIR /app

COPY go.* ./

RUN go mod download

COPY *.go ./

RUN go build -o /desafio-go

ENTRYPOINT ["/desafio-go"]
